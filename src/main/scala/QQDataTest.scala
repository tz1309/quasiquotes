import spatial.dsl._

import scala.collection.mutable.ListBuffer
import scala.reflect.runtime.{currentMirror, universe}
import scala.tools.reflect.ToolBox
import scala.reflect.runtime.universe._

object Server{
  val stateHolder: ListBuffer[argon.State] = new ListBuffer[argon.State]()
  val dramHolder: ListBuffer[DRAM1[_]] = new ListBuffer[DRAM1[_]]()
  val seqHolder: ListBuffer[Seq[scala.Double]] = new ListBuffer[Seq[scala.Double]]()
  val toolbox: ToolBox[universe.type] = currentMirror.mkToolBox()

  def setMemToDRAM(a: DRAM1[Bits[_]], v: scala.Array[scala.Double])(implicit state: argon.State): Unit = {
  }
}



@spatial object QQDataTest extends SpatialApp {
  def main(args: Array[String]): Unit = {
    val a = scala.Seq.tabulate[scala.Double](32)(i => i)
    Server.seqHolder.append(a)

    type T = FixPt[TRUE, _16, _16]
    val dram0 = DRAM[T](32.to[I32])
    val dram1 = DRAM[T](32.to[I32])

    Server.stateHolder.append(IR)
    val sign = TypeName("TRUE")
    val intBits = TypeName("_16")
    val fracBits = TypeName("_16")
    val dT = TypeName("FixPt")
    val qq = tq"spatial.lang.$dT[$sign, $intBits, $fracBits]"
    Server.dramHolder.append(dram0)
    val tr: Tree =
      q"""import spatial.dsl._;
         implicit val state = Server.stateHolder.head;
         setMem(Server.dramHolder.head.asInstanceOf[DRAM1[$qq]],
         Array.fromSeq[$qq](Server.seqHolder.head.map(i => i.to[$qq])));"""
    Server.toolbox.eval(tr)

    Accel {
      val sram = SRAM[T](32.to[I32])
      sram load dram0
      dram1 store sram
    }

    printArray(getArray(dram1))
  }

}
