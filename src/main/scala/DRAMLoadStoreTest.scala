import spatial.dsl._

@spatial object DRAMLoadStoreTest extends SpatialApp {
  def main(args: Array[String]): Unit = {
    type F = FixPt[TRUE, _16, _16]
    val len = 32.to[I32];
    val memLen = 16.to[I32]

    val inData = Array.tabulate[F](len){ i => i.to[F] }
    val inDRAM: DRAM1[F] = DRAM[F](len)
    val outDRAM = DRAM[F](len)
    setMem(inDRAM, inData)

    Accel {
      val mem = SRAM[F](memLen)
      Sequential.Foreach(len by memLen) { i =>
        mem load inDRAM(i :: i + memLen)
        Foreach(memLen by 1.to[I32]) { j =>
          mem(j) = mem(j) + 3.02.to[F]
        }

        outDRAM(i :: i + memLen) store mem
      }
    }

    val outData = getMem(outDRAM)
    printArray(outData)
  }
}
